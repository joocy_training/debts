package com.joocy.iou

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.joocy.iou.data.Debt
import kotlinx.android.synthetic.main.activity_ioulist.*
import kotlinx.android.synthetic.main.content_ioulist.*
import java.math.BigDecimal
import java.text.NumberFormat
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class DebtListActivity : AppCompatActivity() {

    private lateinit var debtViewModel: DebtViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_ioulist)

        val adapter = DebtListAdapter(this)
        val debtListView = debtList
        debtListView.adapter = adapter
        debtListView.layoutManager = LinearLayoutManager(this)

        debtViewModel = ViewModelProviders.of(this).get(DebtViewModel::class.java)
        debtViewModel.debts.observe(this, Observer {
                debts -> debts?.let {adapter.setDebts(it)}
        })

        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

}

class DebtListAdapter internal constructor(context: Context): RecyclerView.Adapter<DebtListAdapter.DebtViewHolder>() {

    private val inflater = LayoutInflater.from(context)
    private var debts = emptyList<Debt>()

    inner class DebtViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val creditorItemView = itemView.findViewById<TextView>(R.id.creditor)
        val amountItemView = itemView.findViewById<TextView>(R.id.amount)
        val dateItemView = itemView.findViewById<TextView>(R.id.date)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DebtViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_debt_item, parent, false)
        return DebtViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: DebtViewHolder, position: Int) {
        val current = debts[position]
        holder.creditorItemView.text = current.creditor
        holder.amountItemView.text = formatAmount(current.amount)
        holder.dateItemView.text = formatDate(current.date)
    }

    override fun getItemCount() = debts.size

    fun setDebts(debts: List<Debt>) {
        this.debts = debts
        notifyDataSetChanged()
    }

    private fun formatAmount(amount: BigDecimal): String {
        return NumberFormat.getCurrencyInstance().format(amount)
    }

    private fun formatDate(date: ZonedDateTime): String {
        val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
        return formatter.format(date)
    }
}