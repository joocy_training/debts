package com.joocy.iou.data.converter

import android.arch.persistence.room.TypeConverter
import java.math.BigDecimal

object MoneyTypeConverter {

    @TypeConverter
    @JvmStatic
    fun moneyToString(value: BigDecimal) = value.toString()

    @TypeConverter
    @JvmStatic
    fun stringToMoney(value: String) = BigDecimal(value)

}