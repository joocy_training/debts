package com.joocy.iou.data.converter

import android.arch.persistence.room.TypeConverter
import java.time.ZonedDateTime

object DateTypeConverter {

    @TypeConverter
    @JvmStatic
    fun stringToDate(value: String) = ZonedDateTime.parse(value)

    @TypeConverter
    @JvmStatic
    fun dateToString(date: ZonedDateTime) = date.toString()

}