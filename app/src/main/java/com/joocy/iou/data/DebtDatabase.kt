package com.joocy.iou.data

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.joocy.iou.data.converter.DateTypeConverter
import com.joocy.iou.data.converter.MoneyTypeConverter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.time.ZonedDateTime

@Database(entities = [Debt::class], version = 1)
@TypeConverters(DateTypeConverter::class, MoneyTypeConverter::class)
abstract class DebtDatabase : RoomDatabase() {

    abstract fun debtDAO(): DebtDAO

    companion object {
        private var DATABASE: DebtDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): DebtDatabase {
            if (DATABASE == null) {
                DATABASE = Room.databaseBuilder(context.applicationContext,
                    DebtDatabase::class.java,
                    "debt_database")
                    .addCallback(DebtDatabaseCallback(scope))
                    .build()
            }
            return DATABASE as DebtDatabase
        }
    }

    private class DebtDatabaseCallback(private val scope: CoroutineScope): RoomDatabase.Callback() {

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            DATABASE?.let { database ->
                scope.launch(Dispatchers.IO) {
                    populdateDatabase(database.debtDAO())
                }
            }
        }

        fun populdateDatabase(dao: DebtDAO) {
            dao.removeAll()

            var debt = Debt(1, ZonedDateTime.now().minusMonths(2), "Mum", BigDecimal("12.50"))
            dao.add(debt)
            debt = Debt(2, ZonedDateTime.now().minusWeeks(2), "Dad", BigDecimal("350.00"))
            dao.add(debt)
        }

    }
}
