package com.joocy.iou.data

import android.support.annotation.WorkerThread

class DebtRepository(private val debtDao: DebtDAO) {

    val debts = debtDao.all()

    @WorkerThread
    fun add(debt: Debt) = debtDao.add(debt)

}