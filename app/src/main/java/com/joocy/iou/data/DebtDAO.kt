package com.joocy.iou.data

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

@Dao
interface DebtDAO {

    @Query("SELECT * FROM debts ORDER BY date ASC")
    fun all(): LiveData<List<Debt>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(debt:Debt)

    @Delete
    fun remove(debt:Debt)

    @Query("DELETE FROM debts")
    fun removeAll()
}