package com.joocy.iou.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.math.BigDecimal
import java.time.ZonedDateTime

@Entity(tableName = "debts")
data class Debt(
    @PrimaryKey val id: Long,
    val date: ZonedDateTime,
    val creditor: String,
    val amount: BigDecimal)